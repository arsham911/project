import json
import os
import sys
import requests

MODE = 'FILE'  # режим получения сообщений (FILE - чтение из локального файла, API - получение по API)
API_ENDPOINT = 'https://raw.githubusercontent.com/thewhitesoft/student-2022-assignment/main/data.json'  # ссылка API
SOURCE_FN = r'Data\data.json'  # путь к файлу с сообщениями. r в начале строки чтобы не экранировать слеш в пути
REPLACEMENT_FN = r'Data\replacement.json'  # путь к файлу с заменами.
RESULT_FN = r'Data\result.json'  # куда сохранять готовый файл


def get_list_from_api():
    """
    Получить сообщения через API
    :return: список полученных сообщений
    """
    try:
        resp = requests.get(API_ENDPOINT).json()
    except Exception as e:
        print(f'Ошибка получения информации через API: {e}')
        return None

    print('Информация по API успешно получена!')
    return resp


def get_list_from_file(fn: str):
    """
    Получить сообщения из файла.
    :param fn: имя файла со списком сообщений
    :return: список полученных сообщений
    """
    if not os.path.isfile(fn):
        print(f'Не найден файл с данными "{fn}"')
        return None

    try:
        with open(fn, 'r') as file:
            json_data = json.load(file)
    except Exception as e:
        print(f'Ошибка чтения файла с JSON: {str(e)}')
        return None

    print(f'Информация из файла "{fn}" успешно прочитана!')
    return json_data


def get_source_list():
    """
    В зависимости от выбранного режима получить список сообщений
    :return: список полученных сообщений
    """
    if MODE == 'API':
        source_json = get_list_from_api()
    elif MODE == 'FILE':
        source_json = get_list_from_file(SOURCE_FN)
    else:
        print('Не удалось распознать выбранный режим получения сообщений (API или FILE). Работа невозможна!')
        return None

    return source_json


def save_to_json(fn: str, my_list: list):
    """
    Сохранить результат
    :param fn: куда сохранять
    :param my_list: список для сохранения
    :return:
    """
    try:
        with open(fn, 'w') as file:
            json.dump(my_list, file)
        print(f'Результат успешно сохранен в "{fn}"')
        return True
    except Exception as e:
        print(f'Не удалось сохранить файл результата. Ошибка: {e}')
        return False


def sort_list_of_dict(my_list: list):
    """
    Отсортировать текущий список замен по длине значения ключа 'replacement'
    :param my_list: список словарей
    :return:
    """
    return my_list.sort(key=lambda dictionary: len(dictionary['replacement']), reverse=True)


def get_last_source(replacement_list: list, current_replacement: str):
    """
    Получить последний встречающийся source (если повторяется replacement)
    :param replacement_list: список всех замен. каждый элемент - словарь
    :param current_replacement: текущая замена, для которой ищем последний "source"
    :return:
    """
    last_source = None
    for replacement_dict in replacement_list:
        replacement = replacement_dict.get('replacement')
        if replacement == current_replacement:
            last_source = replacement_dict.get('source')

    return last_source


def restore_message(source_str: str, replacement_list: list):
    for replacement_dict in replacement_list:
        replacement = replacement_dict.get('replacement')  # что меняем.
        source = get_last_source(replacement_list, replacement)  # на что меняем (последний встречающийся)
        if replacement in source_str:
            if not source:  # если замена встречается, но source пустой, то такая строка нам не нужна
                return None
            print(f'В исходном сообщении "{source_str}" произведена замена: "{replacement}" -> "{source}"')
            source_str = source_str.replace(replacement, source)

    return source_str


def main():
    source_list = get_source_list()
    if not source_list:
        sys.exit()

    replacements_list = get_list_from_file(REPLACEMENT_FN)  # получить список всех замен
    if not replacements_list:
        sys.exit()

    sort_list_of_dict(replacements_list)  # отсортировать полученный список

    restored_messages = []
    for source_str in source_list:
        if not source_str:
            continue

        restored_message = restore_message(source_str, replacements_list)  # получить восстановленное сообщение
        if restored_message:  # в случае успешного получения - добавить в список для последующего сохранения
            restored_messages.append(restored_message)

    save_to_json(RESULT_FN, restored_messages)


if __name__ == '__main__':
    main()